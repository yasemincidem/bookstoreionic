var base = 'http://localhost/bookstore/api/values';

angular.module('BookStoreMobile.services', [])

.factory('Loader', ['$ionicLoading', '$timeout', function ($ionicLoading, $timeout) {

    var LOADERAPI = {

        showLoading: function (text) {
            text = text || 'Loading...';
            $ionicLoading.show({
                template: text
            });
        },

        hideLoading: function () {
            $ionicLoading.hide();
        },

        toggleLoadingWithMessage: function (text, timeout) {
            var self = this;

            self.showLoading(text);

            $timeout(function () {
                self.hideLoading();
            }, timeout || 3000);
        }

    };
    return LOADERAPI;
}])

.factory('BooksFactory', ['$http', function ($http) {

    var BooksFactory = {};
    BooksFactory.getBookList = function () {
        return $http.get(base + '/BookList');
    },
	BooksFactory.getBookListById = function (id) {
	    return $http({
	        method: 'GET',
	        url: base + '/BookListById?id=' + id,
	    })
	}
    return BooksFactory;
}])
.factory('ReservationFactory', ['$http', function ($http) {
    var ReservationFactory = {};
    ReservationFactory.addToReserv = function (reserv) {
        return $http({
            method: 'POST',
            url: base + '/AddToReservations',
            data: {
                BookID: reserv.bookId,
                CheckinDate: reserv.CheckinDate,
                CheckoutDate: reserv.CheckoutDate
            }
        })
    }
    return ReservationFactory;
}])
.factory('CartFactory', ['$http',
function ($http) {
    var CartFactory = {};
    CartFactory.addToCart = function (book) {
        return $http({
            method: 'POST',
            url: base + '/AddToCarts',
            data: {
                bookId: book.bookId,
                title: book.title,
                short_description: book.short_description,
                rating: book.rating,
                price: book.price
            }
        })
    },
	CartFactory.getCartItems = function () {
	    return $http.get(base + '/CartList');
	}
    return CartFactory;
}
])
.factory('RegisterFactory', ['$http',
function ($http) {
    var RegisterFactory = {};
    RegisterFactory.register = function (user) {
        return $http({
            method: 'POST',
            url: base + '/register',
            data: {
                email: user.email,
                name: user.name,
                password: user.password
            }
        })
    },
	RegisterFactory.login = function (user) {
	    return $http({
	        method: 'POST',
	        url: base + '/login',
	        data: {
	            email: user.email,
	            password: user.password
	        }
	    })
	}
    return RegisterFactory;
}
])