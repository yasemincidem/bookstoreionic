
var base = 'http://localhost/bookstore/api/values';

angular.module('BookStoreMobile.services', [])

.factory('Loader', ['$ionicLoading', '$timeout', function ($ionicLoading, $timeout) {

	var LOADERAPI = {

		showLoading: function (text) {
			text = text || 'Loading...';
			$ionicLoading.show({
				template: text
			});
		},

		hideLoading: function () {
			$ionicLoading.hide();
		},

		toggleLoadingWithMessage: function (text, timeout) {
			var self = this;

			self.showLoading(text);

			$timeout(function () {
				self.hideLoading();
			}, timeout || 3000);
		}

	};
	return LOADERAPI;
}])
	.factory('LSFactory', [function() {
		var LSAPI = {
			clear: function() {  
				return localStorage.clear(); 
			},
			get: function (key) {
				if (typeof localStorage["fav"] !== "undefined"
                   && localStorage["fav"] !== "undefined") {
					return JSON.parse(localStorage.getItem(key));
				}
			},
			set: function(key, data) {   
				return localStorage.setItem(key,  JSON.stringify(data));     
			},
			delete: function(key) {   
				return localStorage.removeItem(key);    
			},
			getAll: function() {  
				var books = [];        
				var items = Object.keys(localStorage);
				for (var i = 0; i < items.length; i++) {      
					if (items[i] !== 'user' || items[i] != 'token') {
						if (typeof localStorage["fav"] !== "undefined"
                   && localStorage["fav"] !== "undefined") {
							books.push(JSON.parse(localStorage[items[i]]));
						}
					}      
				}           
				return books;    
			}
		};
		return LSAPI;
	}])


	.factory('AuthFactory', ['LSFactory', function(LSFactory) {
		var userKey = 'user';
		var tokenKey = 'token';
		var AuthAPI = {
			isLoggedIn: function() {         
				return this.getUser() == null ? false : true;  
			},
			getUser: function() {    
				return LSFactory.get(userKey);
			},
			setUser: function(user) {
				return LSFactory.set(userKey, user); 
			},
			getToken: function() { 
				return LSFactory.get(tokenKey);      
			},
			setToken: function(token) {    
				return LSFactory.set(tokenKey, token);    
			},
			deleteAuth: function() {  
				LSFactory.delete(userKey);      
			}
		};
		return AuthAPI;
	}])
	// This token is the only way the server knows if the user is authenticated and if the user is authorized to view the content. 
//.factory('TokenInterceptor', ['$q', 'AuthFactory', function ($q, AuthFactory) {
//	return {
//		request: function (config) {
//			config.headers = config.headers || {};
//			var token = AuthFactory.getToken();
//			var user = AuthFactory.getUser();
//			if (token && user) {
//				config.headers['X-Access-Token'] = token.token;
//				config.headers['X-Key'] = user.email;
//				config.headers['Content-Type'] = "application/json";
//			}
//			return config || $q.when(config);
//		},
//		response: function (response) {
//			return response || $q.when(response);
//		}
//	};
//}])

.factory('BooksFactory', ['$http', function ($http) {

	var BooksFactory = {};
	BooksFactory.getBookList = function () {
		return $http.get(base + '/BookList');
	},
	BooksFactory.getBookListById = function (id) {
			return $http({
				method: 'GET',
				url: base + '/BookListById?id='+id,
			})
		}
	return BooksFactory;
}])
.factory('CartFactory', ['$http', 'AuthFactory',
function ($http, AuthFactory) {
	var CartFactory = {};
	CartFactory.addToCart = function (book) {
		return $http({
			method: 'POST',
			url: base + '/AddToCarts',
			data: {
				bookId: book.bookId,
				title: book.title,
				short_description: book.short_description,
				rating: book.rating,
				price:book.price
			}
		})
	},
	CartFactory.getCartItems = function () {
		return $http.get(base + '/CartList');
	}
	return CartFactory;
}
])
.factory('RegisterFactory', ['$http', 'AuthFactory',
function ($http, AuthFactory) {
	var RegisterFactory = {};
	RegisterFactory.register = function (user) {
		return $http({
			method: 'POST',
			url: base + '/register',
			data: {
				email: user.email,
				name: user.name,
				password: user.password
			}
		})
	},
	RegisterFactory.login = function (user) {
		return $http({
			method: 'POST',
			url: base + '/login',
			data: {
				email: user.email,
				password: user.password
			}
		})
	}
	RegisterFactory.logout = function () {
		AuthFactory.deleteAuth();
	}

	return RegisterFactory;
}
])